from django.conf.urls.defaults import patterns
from marketing.sitemap import SITEMAPS

urlpatterns = patterns('django.contrib.sitemaps.views',
    (r'^sitemap\.xml$', 'index', {'sitemaps': SITEMAPS}),
    (r'^sitemap-(?P<section>.+)\.xml$', 'sitemap', {'sitemaps': SITEMAPS}),
)