function addProductReview(){
    // build an object of review data to submit
    var review = {
        title: jQuery("#id_title").val(),
        content: jQuery("#id_content").val(),
        rating: jQuery("#id_rating").val(),
        slug: jQuery("#id_slug").val() };
    // make request, process response
    jQuery.post("/review/product/add/", review,
        function(response){
            jQuery("#review_errors").empty();
            // evaluate the "success" parameter
            if(response.success == "True"){
                // disable the submit button to prevent duplicates
                jQuery("#submit_review").attr('disabled','disabled');
                // if this is first review, get rid of "no reviews" text
                jQuery("#no_reviews").empty();
                // add the new review to the reviews section
                jQuery("#reviews").prepend(response.html).slideDown();
                // get the newly added review and style it with color
                var new_review = jQuery("#reviews").children(":first");
                //new_review.addClass('new_review');
                new_review.effect('highlight', 5000);
                // hide the review form
                jQuery("#review_form").slideToggle();
            }
            else{
                // add the error text to the review_errors div
                jQuery("#review_errors").append(response.html);
            }
        }, "json");
}

// toggles visibility of "write review" link
// and the review form.
function slideToggleReviewForm(){
    jQuery("#review_form").slideToggle();
    jQuery("#add_review").slideToggle();
}

function addTag(){
    var tag = { tag: jQuery("#id_tag").val(),
        slug: jQuery("#id_slug").val() };
    jQuery.post("/tag/product/add/", tag,
        function(response){
            if (response.success == "True"){
                jQuery("#tags").empty();
                jQuery("#tags").append(response.html);
                jQuery("#id_tag").val("");
            }
        }, "json");
}

function statusBox(){
    jQuery('<div id="loading">Loading...</div>')
        .prependTo("#main")
        .ajaxStart(function(){jQuery(this).show();})
        .ajaxStop(function(){jQuery(this).hide();})
}

function addToCart(){
    var cart = { quantity: $("#id_quantity").val(),
        product_slug: $("#id_product_slug").val()}; // this is sent directly to show_product
    var mypost = jQuery.post(".", cart,
        function(response){
            if (response.success == "True"){
                jQuery("#cart-errors").empty();
                jQuery("#cart-items-count").text(response.qty);
                jQuery("#cart-subtotal").text(response.subtotal);
                jQuery('<div id="added-to-cart">Product added to cart!</div>')
                    .prependTo("#main")
                    .fadeIn(2000)
                    .fadeOut(2000);
                jQuery('#product_image').effect('transfer',
                    {to: "#cart_box", className: "ui-effects-transfer"}, 1000);
            }
            else {
                var message = "", obj = response.errors;
                for (var i in obj) {
                    if (!response.errors.hasOwnProperty(i)) continue;
                    message += obj[i];
                }
                jQuery("#cart-errors").text(message);
            }
        }, "json");
}

function prepareDocument(){
    //****************************************
    $(document).ajaxSend(function(event, xhr, settings) {
        function getCookie(name) {
            var cookieValue = null;
            if (document.cookie && document.cookie != '') {
                var cookies = document.cookie.split(';');
                for (var i = 0; i < cookies.length; i++) {
                    var cookie = jQuery.trim(cookies[i]);
                    // Does this cookie string begin with the name we want?
                    if (cookie.substring(0, name.length + 1) == (name + '=')) {
                        cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                        break;
                    }
                }
            }
            return cookieValue;
        }
        function sameOrigin(url) {
            // url could be relative or scheme relative or absolute
            var host = document.location.host; // host + port
            var protocol = document.location.protocol;
            var sr_origin = '//' + host;
            var origin = protocol + sr_origin;
            // Allow absolute or scheme relative URLs to same origin
            return (url == origin || url.slice(0, origin.length + 1) == origin + '/') ||
                (url == sr_origin || url.slice(0, sr_origin.length + 1) == sr_origin + '/') ||
                // or any other URL that isn't scheme relative or absolute i.e relative.
                !(/^(\/\/|http:|https:).*/.test(url));
        }
        function safeMethod(method) {
            return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
        }

        if (!safeMethod(settings.type) && sameOrigin(settings.url)) {
            xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
        }
    });
    //****************************************
    //prepare the search box
    jQuery("form#search").submit(function(){
        var text = jQuery("#id_q").val();
        if (text == "" || text == "Search"){
            alert("Enter a search term.");
            return false;
        }
    });
    //prepare product review form
    jQuery("#submit_review").click(addProductReview);
    jQuery("#review_form").addClass('hidden');
    jQuery("#add_review").click(slideToggleReviewForm);
    jQuery("#add_review").addClass('visible');
    jQuery("#cancel_review").click(slideToggleReviewForm);
    jQuery("#add_tag").click(addTag);
    jQuery("#id_tag").keypress(function(event){
        if (event.keyCode == 13 && jQuery("#id_tag").val().length > 2){
            addTag();
            event.preventDefault();
        }
    });
    jQuery("#add_to_cart").click(addToCart);
    jQuery("#id_quantity").keypress(function(event){
        if (event.keyCode == 13){
            addToCart();
            event.preventDefault();
        }
    });

    statusBox();
}

jQuery(document).ready(prepareDocument);