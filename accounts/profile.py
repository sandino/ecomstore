from django.contrib.auth.models import User
from django.db.models.signals import post_save, post_delete
from models import UserProfile
from forms import UserProfileForm

def set(request):
    profile = retrieve(request)
    profile_form = UserProfileForm(request.POST, instance=profile)
    profile_form.save()

def create_profile(sender, **kw):
    user = kw["instance"]
    if kw["created"]:
        up = UserProfile(user=user)
        up.save()


post_save.connect(create_profile, sender=User, dispatch_uid="users.profilecreation")
