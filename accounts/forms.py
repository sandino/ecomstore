from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.utils.translation import ugettext_lazy as _
from models import UserProfile

class UserProfileForm(forms.ModelForm):
    class Meta:
        model = UserProfile
        exclude = ('user',)


class RegistrationForm(UserCreationForm):
    password1 = forms.RegexField(label="Password", regex=r'^(?=.*\W+).*$',
        help_text=_('Password must be six characters long\
        and contain at least one non-alphanumeric character.'),
        widget=forms.PasswordInput, min_length=6,
        #render_value=False
    )
    password2 = forms.RegexField(label=_("Password confirmation"), regex=r'^(?=.*\W+).*$',
        widget=forms.PasswordInput, min_length=6,
        #render_value=False
    )
    email = forms.EmailField(max_length="50")